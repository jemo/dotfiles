#!/bin/sh
#     _ _____ __  __  ___  
#    | | ____|  \/  |/ _ \ 
# _  | |  _| | |\/| | | | |
#| |_| | |___| |  | | |_| |
# \___/|_____|_|  |_|\___/ 
#
nitrogen --restore &
nm-applet &
sh ~/scripts/yt_downloader.sh &
sh ~/scripts/weather.sh &
