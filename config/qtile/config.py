# -*- coding: utf-8 -*-
#     _ _____ __  __  ___  
#    | | ____|  \/  |/ _ \ 
# _  | |  _| | |\/| | | | |
#| |_| | |___| |  | | |_| |
# \___/|_____|_|  |_|\___/ 
# Inspirations :
# https://github.com/arcolinux/arcolinux-qtile/blob/master/etc/skel/.config/qtile/config.py
# https://gitlab.com/dwt1/dotfiles/-/blob/master/.config/qtile/config.py
#                          
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import re
import socket
import subprocess

from typing import List  # noqa: F401

from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, Screen, Match
from libqtile.lazy import lazy
#Fonction non disponible dans cette version
#from libqtile.utils import guess_terminal

# Settings/helpers
from settings import COLS

mod          = "mod4"

# Software
terminal     = "terminator"
fichiers     = "caja"
browser      = "firefox"
email_client = "thunderbird"
text_editor  = "geany"

keys = [
    Key([mod], "k", lazy.layout.down(),
        desc="Move focus down in stack pane"),
    Key([mod], "j", lazy.layout.up(),
        desc="Move focus up in stack pane"),

    Key([mod, "control"], "k", lazy.layout.shuffle_down(),
        desc="Move window down in current stack "),
    Key([mod, "control"], "j", lazy.layout.shuffle_up(),
        desc="Move window up in current stack "),

    Key([mod], "space", lazy.layout.next(),
        desc="Switch window focus to other pane(s) of stack"),

    Key([mod, "shift"], "space", lazy.layout.rotate(),
        desc="Swap panes of split stack"),

    Key([mod], "h", lazy.layout.grow()),
    Key([mod], "l", lazy.layout.shrink()),
    Key([mod], "n", lazy.layout.normalize()),
    Key([mod], "f", lazy.window.toggle_fullscreen()),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),

# Applications
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key([mod], "e", lazy.spawn(fichiers), desc="Launch file explorer"),
    Key([mod], "b", lazy.spawn(browser), desc="Launch browser"),
    Key([mod], "m", lazy.spawn(email_client), desc="Launch email client"),
    Key([mod], "g", lazy.spawn(text_editor), desc="Launch text editor"),
    # TODO à revoir
    Key([mod], "i", lazy.spawn(terminal+" -e sh /home/jeremy/scripts/wifi.sh"), 
                    desc="Redémarrage wifi"),
    Key([mod], "v", lazy.spawn("caja /home/jeremy/Vidéos/fichiers/"), 
                    desc="Dossier des vidéos"),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "control"], "r", lazy.restart(), desc="Restart qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown qtile"),
    Key([mod], "r", lazy.spawncmd(),
        desc="Spawn a command using a prompt widget"),

# INCREASE/DECREASE BRIGHTNESS
    Key([], "XF86MonBrightnessUp", lazy.spawn("xbacklight -inc 5")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("xbacklight -dec 5")),

# INCREASE/DECREASE/MUTE VOLUME
    Key([], "XF86AudioMute", lazy.spawn("amixer -q set Master toggle")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer -q set Master 5%-")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer -q set Master 5%+")),

# Navigation
    Key([mod], "Left", lazy.screen.prev_group()),
    Key([mod], "Right", lazy.screen.next_group()),
]

groups = [
    Group(name="🌐 Web",
          spawn=["firefox"]
          #exclusive=True,
          #layouts=["Max","MonadTall"],
          #persist=False
          ),
    Group(name="🖱</>",
          spawn=[terminal]
          #exclusive=True,
          #layouts=["Max","MonadTall"],
          #persist=False
          ),
    Group("📽 Misc"
          ),
]

layouts = [
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Columns(),
    # layout.Matrix(),
    layout.MonadWide(),
    layout.MonadTall(margin=2, 
                     border_width=3, 
                     border_focus="#5e81ac", 
                     border_normal="#4c566a"),
        # ~ border_width=0,
        # ~ max_ratio=1,
        # ~ min_ratio=0
        # ~ ),
    layout.Max(),
    layout.Floating(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

##### PROMPT #####
prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())

def sendmessage(message):                                            
    subprocess.Popen(["notify-send", message])
    return

# Recuperer les températures
def get_weather():
    file_weather = open("/home/jeremy/.local/share/weatherreportshort", "r")
    # Lire le fichier et enlever le saut de ligne
    weatherreport = file_weather.read().split("\n")[0]
    file_weather.close()
    return weatherreport

def weather_button3(self):
    sendmessage("🌈 Weather module ☔ Chance of rain/snow 🥶 Daily low 🌞 Daily high")

def update_button1(self):
    lazy.spawn(terminal+" -e sudo apt upgrade")

widget_defaults = dict(
    font='Liberation Mono',
    fontsize=12,
    padding=0,
    background = COLS["dark_4"])

extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
               widget.GroupBox(
                   font="Liberation Mono Bold",
                   other_current_screen_border=COLS["orange_0"],
                   this_current_screen_border=COLS["blue_0"],
                   other_screen_border=COLS["orange_0"],
                   this_screen_border=COLS["blue_0"],
                   highlight_color=COLS["blue_0"],
                   urgent_border=COLS["red_1"],
                   background=COLS["dark_4"],
                   highlight_method="line",
                   inactive=COLS["dark_2"],
                   active=COLS["light_2"],
                   disable_drag=True,
                   borderwidth=4
                   ),
               widget.Sep(
                   linewidth = 2,
                   padding = 5
                   ),
               widget.Prompt(
                   prompt=prompt,
                   cursor_color=COLS["light_3"]
                   ),
               widget.WindowName(),
               widget.CurrentLayoutIcon(
                   custom_icon_paths=[os.path.expanduser("~/.config/qtile/icons")],
                   padding = 0,
                   scale=0.7
                   ),
               widget.Systray(),
               widget.Sep(
                   linewidth = 2,
                   padding = 5
                   ),
               widget.GenPollText(
                   func=get_weather,
                   mouse_callbacks={"Button3": weather_button3},
                   update_interval=60,
                   fontsize=11
                   ),
               widget.Sep(
                   linewidth = 2,
                   padding = 5
                   ),
               widget.Net(
                   interface="wlo1",
                   format="{down}🔺🔻{up}",
                   update_interval=5
                   ),
               widget.Sep(
                       linewidth = 2,
                       padding = 5
                       ),
               widget.CheckUpdates(
                       colour_have_updates=COLS["red_0"],
                       colour_no_updates=COLS["green_0"],
                       distro="Ubuntu",
                       custom_command="apt list --upgradable | grep /",
                       display_format="↺ {updates}",
                       #mouse_callbacks={"Button1": update_button1},
                       #execute=lazy.spawn(terminal+" -e sudo apt upgrade")
                       update_interval=600
                       ),
               widget.Sep(
                       linewidth = 2,
                       padding = 5
                       ),
               widget.TextBox(
                       text="🌡"
                       ),
               widget.ThermalSensor(
                       metric = True,
                       padding = 3,
                       threshold = 80
                       ),
               widget.Sep(
                       linewidth = 2,
                       padding = 5
                       ),
               widget.TextBox(
                       text="☼",
                       padding = 0,
                       fontsize=20
                       ),
               widget.Backlight(
                       backlight_name="intel_backlight",
                       update_interval=1
                       ),
               widget.Sep(
                       linewidth = 2,
                       padding = 5
                       ),
               widget.Battery(
                       charge_char="🔌",
                       discharge_char="🔋",
                       full_char="⚡",
                       empty_char="❗",
                       format="{char}{percent:2.0%}"
                       ),
               widget.Sep(
                       linewidth = 2,
                       padding = 5
                       ),
               widget.Clock(format='%a %d %b %Y, %H:%M'),
               widget.Sep(
                       linewidth = 2,
                       padding = 5
                       ),
               widget.QuickExit(
                       countdown_format="[ {} ]",
                       countdown_start=4,
                       default_text="Quitter"
                       ),
            ],
            24,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
])
auto_fullscreen = True
focus_on_window_activation = "smart"

@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.call([home])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"

