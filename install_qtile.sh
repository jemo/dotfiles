#!/bin/sh
#     _ _____ __  __  ___  
#    | | ____|  \/  |/ _ \ 
# _  | |  _| | |\/| | | | |
#| |_| | |___| |  | | |_| |
# \___/|_____|_|  |_|\___/ 
#script d'installation de qtile 

ln -sfv $path/config/qtile $HOME/.config/qtile

pip3 install xcffib
pip3 install --no-cache-dir cairocffi
sudo apt install libpangocairo-1.0-0 python3-dbus xbacklight nitrogen
pip3 install qtile

cat > /usr/share/xsessions/qtile.desktop << EOF
[Desktop Entry]
Name=Qtile GNOME
Comment=Tiling window manager
Exec=qtile
Type=XSession
EOF
