#     _ _____ __  __  ___  
#    | | ____|  \/  |/ _ \ 
# _  | |  _| | |\/| | | | |
#| |_| | |___| |  | | |_| |
# \___/|_____|_|  |_|\___/ 
autoload -U colors && colors

eval "$(starship init zsh)"

setopt histignorealldups sharehistory

# Keep 10000 lines of history within the shell
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.bash_history

setopt hist_ignore_all_dups # remove older duplicate entries from history
setopt hist_reduce_blanks   # remove superfluous blanks from history items
setopt inc_append_history   # save history entries as soon as they are entered
setopt share_history        # share history between different instances of the shell
setopt correct_all          # autocorrect commands
setopt auto_list            # automatically list choices on ambiguous completion
setopt auto_menu            # automatically use menu completion
setopt always_to_end        # move cursor to end if word had one match

# Basic auto tab complete
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
autoload compinit && compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
# Include hidden files in autocomplete:
_comp_options+=(globdots)

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2
eval "$(dircolors -b)"
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'
# Ctrl + y pour accepter la suggestions
bindkey '^y' autosuggest-accept

#Alias
alias ls='ls -lhN --color=auto --group-directories-first'
alias la='ls -ahN --color=auto --group-directories-first'
alias pi='ssh pi@192.168.1.66'
alias v='nvim'
alias ex='exit'
alias cl='clear'
alias config="v ~/.zshrc"
alias rm='rm -iv'
alias cp='cp -iv'
alias mv='mv -iv'
alias pyl='pylint3'
alias py='python3'
alias mpy="mpv --ytdl-format='best[height<=1080]'"
alias youdl="sh /home/jeremy/scripts/youdl.sh"
alias startup="sh /home/jeremy/scripts/startup.sh"
alias subs="sh /home/jeremy/scripts/subscriptions.sh"

#automatically run ls after every cd
autoload -U add-zsh-hook
add-zsh-hook -Uz chpwd (){ls; }

# Load zsh-syntax-highlighting; should be last.
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
eval "$(zoxide init zsh)"
