#!/bin/zsh
#     _ _____ __  __  ___  
#    | | ____|  \/  |/ _ \ 
# _  | |  _| | |\/| | | | |
#| |_| | |___| |  | | |_| |
# \___/|_____|_|  |_|\___/ 

SESSIONNAME='base'
tmux has-session -t $SESSIONNAME &> /dev/null

if [ $? != 0 ] 
 then
    tmux new-session  -s $SESSIONNAME -d 
    tmux split-window -h 
    tmux split-window -v
fi

tmux attach -t $SESSIONNAME
