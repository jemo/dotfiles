"     _ _____ __  __  ___  
"    | | ____|  \/  |/ _ \ 
" _  | |  _| | |\/| | | | |
"| |_| | |___| |  | | |_| |
" \___/|_____|_|  |_|\___/ 
"|           _            |
"|   __   __(_)___ ___    |
"|   | | / / / __ `__ \   |
"|   | |/ / / / / / / /   |
"|   |___/_/_/ /_/ /_/    |
"
"
" Configuration pour la gestion des plugin
if empty(glob('~/.vim/autoload/plug.vim'))
	  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
	      \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin()
Plug 'morhetz/gruvbox'
Plug 'Valloric/YouCompleteMe'
Plug 'vim-airline/vim-airline'
call plug#end()

" Configuration de l'affichage

let mapleader=" "
"set lines=50 columns=100 " Taille de la police
"set guifont=Courier_New:h11 " Police et taille de caractère

syntax on                 " syntax highlighting par défaut
syntax enable             " coloration syntaxique
set background=dark       " configuration pour le terminal
set ruler                 " affichage de la position
set number relativenumber " numérotation des lignes
set cursorline            " Marque la ligne sous le curseur
set cursorcolumn          " Marque la ligne sous le curseur
set scrolloff=5           " Affiche un minimum de 5 lignes autour du curseur
set showmatch             " show the matching part of the pair for [] {} and ()
set showcmd               " Show (partial) command in status line.
set wildmenu              " Affiche un menu avec la liste des complétions possible un menu dans la ligne de commande

" Configuration des recherches

" search insensitive to upper or lower case and start search
set ignorecase            " recherche ne prend pas en compte les majuscules
set incsearch             " recherche des le début
set hlsearch              " affichage des recherches
set wildmenu              " Affiche un menu avec la liste des complétions possible un menu dans la ligne de commande
set smartcase             " si recherche contient maj, réactive la sensibilité à la case

set tabstop=4             " tab = 4
set expandtab             " tab = espaces
set bs=2                  " backspace

" Configuration pour Netrw
let g:netrw_liststyle=3   " Affiche les dossiers en arborescence
let g:netrw_banner=0      " Pas de bannière

" Persistent_undo
set undodir=~/.vim/undodir"
set undofile

" Uncomment the following to have Vim jump to the last position when
" reopening a file
"if has("autocmd")
"  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
"endif

" Uncomment the following to have Vim load indentation rules and plugins
" according to the detected filetype.
if has("autocmd")
  filetype plugin indent on
endif

colorscheme gruvbox

" Pour python
set tabstop=4
set softtabstop=4
set shiftwidth=4
"set textwidth=79
set expandtab
set autoindent
set fileformat=unix
set encoding=utf-8

" Pour le plugin Youcompleteme
nnoremap <leader>gl :YcmCompleter GoToDeclaration<CR>
nnoremap <leader>gf :YcmCompleter GoToDefinition<CR>
nnoremap <leader>gg :YcmCompleter GoToDefinitionElseDeclaration<CR>


