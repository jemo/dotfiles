[#](#) Gestion des fichiers de config

## Télechargement des fichiers de config 
`git clone https://framagit.org/jemo/dotfiles.git`

## Lancer le script pour installer et mettre à jour les liens des config
```shell
cd dofiles
sh install_dotfiles.sh
```

## Applications 

- python-pip
- pynvim
- [Neovim](https://neovim.io)
    - [Kickstart.nvim](https://github.com/nvim-lua/kickstart.nvim)
	- Modification dans ~/.config/nvim/init.lua
	    - pyright (lsp server for python)
	    - ajout [vimwiki](https://github.com/vimwiki/vimwiki)
- firefox
- thunderbird
- newsboat
- sxhkd
- light
- keepassx
- mpv
- powertop
- transmission-gtk
- zsh
    - [starship prompt](https://starship.rs/)
- zsh-syntax-highlighting
- zsh-autosuggestions
- yt-dlp
- alacritty
- zoxide

## [QTILE](http://www.qtile.org/)

Pour installer Qtile il faut lancer la commande ci-dessous :

`sudo sh install_qtile.sh`
