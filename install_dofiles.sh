#!/bin/sh
#     _ _____ __  __  ___  
#    | | ____|  \/  |/ _ \ 
# _  | |  _| | |\/| | | | |
#| |_| | |___| |  | | |_| |
# \___/|_____|_|  |_|\___/ 
#Script pour installer les applications dont j'ai besoin

#Déterminer le chemin
path=$(pwd)

## Config des liens symboliques
ln -sfv $path/.bashrc $HOME/.bashrc
ln -sfv $path/.gitconfig $HOME/.gitconfig
ln -sfv $path/.zshrc $HOME/.zshrc
ln -sfv $path/config/starship.toml $HOME/.config/starsship.toml
ln -sfv $path/config/newsboat/config $HOME/.config/newsboat/config
ln -sfv $path/config/amfora $HOME/.config/amfora
ln -sfv $path/config/autostart/sxhkd.desktop $HOME/.config/autostart/sxhkd.desktop
ln -sfv $path/config/alacritty $HOME/.config/alacritty
ln -sfv $path/config/sxhkd $HOME/.config/sxhkd
ln -sfv $path/scripts $HOME/scripts
ln -sfv $path/config/mpv $HOME/.config/mpv

git clone https://github.com/nvim-lua/kickstart.nvim.git "${XDG_CONFIG_HOME:-$HOME/.config}"/nvim
