#!/bin/sh
#     _ _____ __  __  ___  
#    | | ____|  \/  |/ _ \ 
# _  | |  _| | |\/| | | | |
#| |_| | |___| |  | | |_| |
# \___/|_____|_|  |_|\___/ 
# Script pour télécharger des vidéos YouTube

# Définir les chemins des fichiers ici
COOKIES_FILE=~/Vidéos/cookies.txt
CHANNEL_LIST=~/Vidéos/channel_list
CHANNEL_DONE=~/Vidéos/channel_done
ARCHIVES=~/Vidéos/Downloaded.txt

# Notification de début
notify-send "Début Check Videos"

# Fonction pour télécharger une vidéo
yt() {
    yt-dlp --sponsorblock-remove sponsor \
           --cookies "$COOKIES_FILE" \
           --download-archive "$ARCHIVES" \
           -i -o "~/Vidéos/fichiers/%(upload_date)s-%(uploader)s-%(title)s-%(duration_string)s.%(ext)s" \
           --write-thumbnail \
           -f "bestvideo[height<=1080]+bestaudio[ext=mp4]" \
           "$line" --playlist-end 2 --dateafter now-5days && \
           echo "$line" >> "$CHANNEL_DONE"
}

# Vérification si le fichier channel_done est à jour
if [ "$(stat -c %y "$CHANNEL_DONE" 2>/dev/null | cut -d' ' -f1)" != "$(date '+%Y-%m-%d')" ]; then
    echo "" > "$CHANNEL_DONE"
fi

# Lire la liste des chaînes et traiter celles qui ne sont pas encore téléchargées
grep -Fxvf "$CHANNEL_DONE" "$CHANNEL_LIST" | while read -r line; do
    echo "Processing $line"
    yt
done

# Notification de fin
notify-send "Fin Check Videos"
