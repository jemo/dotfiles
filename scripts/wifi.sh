#!/bin/sh
#     _ _____ __  __  ___  
#    | | ____|  \/  |/ _ \ 
# _  | |  _| | |\/| | | | |
#| |_| | |___| |  | | |_| |
# \___/|_____|_|  |_|\___/ 
# Redémarrage du wifi

notify-send 'Redémarrage wifi'

sudo service network-manager restart
