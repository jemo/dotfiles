#!/bin/bash

# Program to read and delete video once they are finished with thumbnail preview

videos_dir="/home/jeremy/Vidéos/fichiers"
selected_file=""
cd "$videos_dir"

# Use fzf to select the video file
selected_file=$(find -type f \( -name "*.mp4" -o -name "*.mkv" -o -name "*.avi" \) | sort -n | fzf --reverse --cycle --border=double)
echo "File selected: $selected_file"

# Check if a file was selected
if [ -n "$selected_file" ]; then
    # Run mpv with verbose messaging
    mpv --msg-level=all=info "$selected_file" 2>&1 | grep -q "End of file"

    # Check if "end of file" message is detected
    if [ $? -eq 0 ]; then
        # Find and process thumbnail files
        find -type f -name "*.webp" | while read -r thumbnail_file; do
            if [ -f "$thumbnail_file" ] && [ "${selected_file%.*}" = "${thumbnail_file%.*}" ]; then
                gio trash "$thumbnail_file"
                echo "Thumbnail file moved to trash: $thumbnail_file"
            fi
        done

        # Move the selected video file to trash
        gio trash "$selected_file"
        echo "Video file moved to trash: $selected_file"
    fi
fi
